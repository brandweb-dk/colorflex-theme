<footer>
  <div class="container">
    <div class="row">
        <div class="col-md-2">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col-md-3 col-md-offset-2 footer-spacer">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>

        <div class="col-md-3 col-md-offset-1">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>
    </div>
  </div>
</footer>
