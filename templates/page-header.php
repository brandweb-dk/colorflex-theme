<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <h1><i class="fa fa-angle-right"></i> <?= Titles\title(); ?></h1>
</div>
