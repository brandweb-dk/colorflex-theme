<div class="container">
  <div class="row">
    <div class="col-md-12 text-intro text-narrow text-center">
      <h1>Vi maler for både private, erhverv og instituioner.</h1>
      <h3>
        Colorflex er en moderne virksomhed, der ofte er underleverandører til virksomheder, der har store opgaver indenfor renovering. Vi er vant til at leve op til kvalitetskrav fra
    institutioner og større virksomheder.
      </h3>
      <hr>
    </div>
  </div>
</div>

<div class="aoc-wrap">
    <div class="container aoc-wrap-padding">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort">
                    <?php if( get_field('billede_venstre') ): ?>

                        <img src="<?php the_field('billede_venstre'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift_venstre'); ?></h2>

                        <p><?php the_field('tekst_venstre'); ?></p>

                        <a class="btn btn-primary btn-outline" href="<?php the_field('button_link_venstre'); ?>"><?php the_field('button_text_venstre'); ?></a>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort">
                    <?php if( get_field('billede_midt') ): ?>

                        <img src="<?php the_field('billede_midt'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift_midt'); ?></h2>

                        <p><?php the_field('tekst_midt'); ?></p>

                        <a class="btn btn-primary btn-outline" href="<?php the_field('button_link_midt'); ?>"><?php the_field('button_text_midt'); ?></a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort">
                    <?php if( get_field('billede_hojre') ): ?>

                        <img src="<?php the_field('billede_hojre'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift_hojre'); ?></h2>

                        <p><?php the_field('tekst_hojre'); ?></p>

                        <a class="btn btn-primary btn-outline" href="<?php the_field('button_link_hojre'); ?>"><?php the_field('button_text_hojre'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
