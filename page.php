<div class="col-md-10 col-md-push-1">
  <div class="page-boxed">
    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('templates/content', 'page'); ?>
    <?php endwhile; ?>
  </div>
</div>
